	 .---.  .--. .-----.   .-.        .-..-..---.  .--. .-..-.
	 : .; :: .--'`-. .-'  .' `.       : `' :: .; :: ,. :: `' :
	 :  _.'`. `.   : :    `. .'.--.   : .. ::   .': :: : `  '
	 : :    _`, :  : :     : :' .; :  : :; :: .; :: :; :.'  `.
	 :_;   `.__.'  :_;     :_;`.__.'  :_;:_;:___.'`.__.':_;:_;


 <br/><h2>Bienvenido al conversor PST a MBOX (para Thunderbird).</h2><br/> 


 <br/>Con el uso de este software usted puede convertir de forma automatica todos los archivos .pst contenidos en una carpeta. <br/>
 
 <br/>El programa deberá ejecutarse bajo consola de comandos (cmd).
 <br/>Para ello deberemos indicarle la ruta (carpeta contenedora) donde se encuentran los .pst de origen así como la ruta donde se creará una carpeta llamada <localFolders> dentro de la que se volcará todo el contenido ya convertido.
 <br/>IMPORTANTE: Deberán mantenerse cerrados los clientes de correo. 
 
    Ejemplo de uso: C:\>PstToMbox.exe "C:\<origen>" "C:\<destino>"
 
 <br/>IMPORTANTE: La carpeta <destino> deberá existir.
 <br/>En este ejemplo, en la carpeta <destino> se creará una carpeta "localFolder", la que podrá copiar en el lugar donde se almacenen sus correos archivados de Thunderbird.
 <br/>En caso de ser la carpeta por defecto, puede encontrarla en C:\Users\<Windows user name>\AppData\Roaming\Thunderbird\Profiles\<Profile name>\Mail, dentro podrá localizar una carpeta localFolders en la cual puede copiar el contenido generado en <destino>\localFolders. 

*******************************************************************************
 <strong> Ejemplo de estructura de archivos:</strong>


  \<origen\> = D:\Correos
<br/> (aqui tendremos nuestros pst originales para migrar) <br/>

  D:\Correos\ <br/>
  -----------2015.pst <br/>
  --------------------Eliminados* <br/>
  --------------------Importante* <br/>
  --------------------------------Procedimientos* <br/>
  --------------------Cursos*<br/>
  -----------2016.pst <br/>
  --------------------Eliminados* <br/>
  --------------------Importante* <br/>
  --------------------Pendiente* <br/>
*Solo visibles desde Outlook <br/>   


  \<destino\> = "D:\Correos Thunderbird"
<br/> (tras el proceso se genera esta estructura que deberemos volcar en la carpeta de Thunderbird anteriormente indicada) <br/>

  D:\Correos Thunderbird\localFolder\ <br/>
| -----------------------------------2015 <br/>
| -----------------------------------2015.sbd\ <br/>
| --------------------------------------------Eliminados <br/>
| --------------------------------------------Importante <br/>
| --------------------------------------------Importante.sbd\ <br/>
| ------------------------------------------------------Procedimientos <br/>
| --------------------------------------------Cursos <br/>
| -----------------------------------2016 <br/>
| -----------------------------------2016.sbd\ <br/>
| --------------------------------------------Eliminados <br/>
| --------------------------------------------Importante <br/>
| --------------------------------------------Pendiente <br/>   
\_______Contenido a copiar<br/><br/>
                       
                        
  Una vez copiado todo el contenido desde D:\Correos Thunderbird\localFolder\, puede abrir Thunderbird y los correos deberán aparecerle ya en las carpetas correspondientes.


*******************************************************************************

Versión de Java requerida: 1.8


author @ pablo ambrosio
// @author: pambrosio  04/05/2016


/*
 
 REFERENCES:
 http://www.coderanch.com/t/660373/Wiki/Accessing-File-Formats
 https://github.com/rjohnsondev/java-libpst
 http://www.programcreek.com/java-api-examples/index.php?api=javax.mail.URLName
 http://stackoverflow.com/questions/8167583/create-an-email-object-in-java-and-save-it-to-file
 https://github.com/benfortuna/mstor
 https://james.apache.org/server/2/apidocs/org/apache/james/mailrepository/MBoxMailRepository.html
 https://java.net/projects/javamail/pages/MboxStore
 http://www.sleuthkit.org/autopsy/docs/api-docs/4.0/_mbox_parser_8java_source.html
 http://www.oracle.com/technetwork/java/javamail/index-138643.html
 https://bugzilla.mozilla.org/show_bug.cgi?id=61815
 attaches : http://stackoverflow.com/questions/34723626/extracting-pst-file-containing-mail-as-an-attachment-of-pstmessage
VIP: http://www.codejava.net/java-ee/javamail/send-e-mail-with-attachment-in-java
http://stackoverflow.com/questions/17156544/sending-an-email-with-an-attachment-using-javamail-api
http://www.tutorialspoint.com/javamail_api/javamail_api_send_email_with_attachment.htm
https://code.google.com/archive/p/java-libpst/
http://stackoverflow.com/questions/2830561/how-to-convert-an-inputstream-to-a-datahandler
http://www.programcreek.com/java-api-examples/index.php?api=javax.mail.util.ByteArrayDataSource
 */

package pstToMbox;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.activation.DataHandler;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.pff.*;


public class PstToMbox {

	//Personal header class
	static class singleHeader
	{
		public String name;
		public String value;

		public singleHeader( String n , String v )
		{
			this.name = n;
			this.value = v;
		}		
	}
	
	//Personal item attach class
	static class singleAttach
	{
		public ByteArrayDataSource arrayData;	
		public String filename;
		public String mime;
		public String cid;
	}
	
	//Create our own email class with the content data
	static class emailBasic
	{
		private List<singleHeader> headers = new ArrayList<singleHeader>();
		private List<singleAttach> attachments = new ArrayList<singleAttach>();	
		private String content = "";
				
		//This process will read a PST format email and insert values in emailBasic class
		public void addPST( PSTMessage email ) throws PSTException, IOException 
		{

			//Treat body content
			if( email.getBodyHTML().isEmpty() )	
			{	
				this.content = email.getBody();	
				this.headers.add( new singleHeader( "Content-Type" , "text/plain; charset=iso-8859-1" ) );
			}
			else	
			{
				this.content = email.getBodyHTML();	
				this.headers.add( new singleHeader( "Content-Type" , "text/html; charset=iso-8859-1" ) );
			}
			
			//Add default Subject
			this.headers.add( new singleHeader( "Subject" , email.getSubject() ) );
			
			//Get headers and initialize them if not
			String stHeaders = email.getTransportMessageHeaders();
			if(stHeaders.isEmpty())
			{
				String from = email.getSenderEmailAddress();
				if( from.lastIndexOf("/cn=")>-1 && (from.lastIndexOf("/cn=") + 4) < from.length() )
				{
					int fromUser = from.lastIndexOf( "/cn=" ) + 4;
					from = from.substring( fromUser );   
				}
				String to = email.getOriginalDisplayTo();
				Date dfdate = email.getCreationTime();
				DateFormat df = new SimpleDateFormat( "EEE, dd MMM yyyy HH:mm:ss" );
				String strDate = df.format(dfdate)+" GMT";
				String subject = email.getSubject();
				stHeaders = "From: " + from + "\r\nTo: " + to + "\r\nDate: " + strDate + "\r\nSubject: " + subject + "\r\nMIME-Version: 1.0";				
			}
			
			//Now we have stHeaders with the string value so need to process it line to line 
			String[] lines = stHeaders.split( System.getProperty( "line.separator" ) );
			int j = 0;
			//Join lines if the same header is divided in some lines
			for ( int i=0;  i<lines.length ; i++ )
			{
				if( lines[i].indexOf(": ") > 1 )	{	j=i;	}
				else if( j != i ){
					lines[j] = lines[j] + lines[i];
					lines[i] = "";
				}
			}
			
			//for each header set it up
			for ( int i=0;  i<lines.length ; i++ )
			{
				if(lines[i].indexOf(": ")>1)
				{
					String headerTmp = lines[i].substring(0,lines[i].indexOf(": "));
					
					if( !headerTmp.toLowerCase().equals("subject") && !headerTmp.toLowerCase().equals("content-type") )
					{
						this.headers.add( new singleHeader( headerTmp , lines[i].substring( lines[i].indexOf( ": " ) + 2 ) ) );

					}
				}
			}
	
			//Status flags treatment and add them like a X-Mozilla-Status header
			int status = 0;
			String strHeader;
			if(email.isRead()){		status+=1;	}
			if(email.isFlagged()){	status+=4;	}
			if(email.hasReplied()){ status+=2;	}		
			if(email.getImportance()==PSTMessage.IMPORTANCE_HIGH)
			{
				if(email.hasForwarded()){ strHeader = "F" + String.format( "%03d" , status ); }
				else{ strHeader = "E" + String.format( "%03d" , status ); }
			}else{
				if(email.hasForwarded()){ status+=1000;	}
				strHeader = String.format( "%04d" , status ) ;
			}				
			this.headers.add( new singleHeader( "X-Mozilla-Status" , strHeader ) );
						
			//At the end, Go through the attached files
			int numberOfAttachments = email.getNumberOfAttachments(); 
			for ( int x = 0 ; x < numberOfAttachments ; x++ ) 
			{ 
				PSTAttachment tmpAttach = email.getAttachment(x); 
				//Here take the long filename, if not, use the short filename
				String filename = tmpAttach.getLongFilename(); 
				if (filename.isEmpty()) { filename = tmpAttach.getFilename(); }
				
				singleAttach attach = new singleAttach();
				tmpAttach.getFileInputStream();
				attach.cid = tmpAttach.getContentId();
				attach.filename = filename;			
				attach.mime = tmpAttach.getMimeTag();
				if( attach.mime.isEmpty() )	{ attach.mime="application/octet-stream"; }										
				attach.arrayData = new ByteArrayDataSource( tmpAttach.getFileInputStream() , attach.mime );				
				
				this.attachments.add( attach );			
			} 
		}	
		
		
		// Process to return this email in MBOX format.
		public Message getMBOX( Session session ) throws MessagingException, IOException
		{
			Message email = new MimeMessage(session);
			
			//Need different treat if it have attachments or not.
			if( this.attachments.isEmpty() )
			{	
				email.setText( this.content );
				for( singleHeader header : this.headers ){	email.setHeader( header.name , header.value );	}					
			}
			else
			{
				//create initial multibody parts. "related" is needed to inline image attach.
				MimeMultipart multipart = new MimeMultipart( "related" );
				BodyPart messageBodyPart = new MimeBodyPart();
				
				//Read each header to set email headers and look for content-type values.
				for(singleHeader header : this.headers)
				{	
					
					if( header.name.toLowerCase().indexOf( "content-type" ) > -1 )//Content-Type
					{
						
						try {
							messageBodyPart.setContent( this.content , header.value );	
							messageBodyPart.setHeader( "Content-Transfer-Encoding" , "binary" );				
						} catch (MessagingException e) {	e.printStackTrace();	}
					}
					try {	email.setHeader( header.name , header.value );	}
					catch (MessagingException e) {	e.printStackTrace();	}
				}
				//attach the text body part
				multipart.addBodyPart( messageBodyPart );
				//Read each attach and add it to the multipart body
				for( singleAttach attach : this.attachments )
				{
					MimeBodyPart attachPart = new MimeBodyPart();
					
					attachPart.setDataHandler( new DataHandler( attach.arrayData ) );
					attachPart.setFileName( attach.filename );
					attachPart.setContentID( "<"  +attach.cid + ">" );
					attachPart.setHeader("Content-Type", attach.mime);
					//System.out.println(attach.mime);
					multipart.addBodyPart( attachPart );
				}
				//add all to email
				email.setContent( multipart );
			}						
			email.saveChanges();
			return email;
		}
	}
	

/********************************************************************** DEFINITIVE **************************************************************************/    
    //
    
	//Scan for PST files in the given folder
    private static void scanPst(File mainFolder,File destinationFolder)
	{
    	//look for the PST files
    	File[] matchingFiles = mainFolder.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.endsWith(".pst");
		    }
		});
    		
    	//Make the main thunderBird folder inside the indicate PST folder
    	File thunderMainFolder = new File( destinationFolder.toString() + "/localFolders" );
    	if ( !thunderMainFolder.exists() ) {	thunderMainFolder.mkdir();	}
    			
		//Start the process for each matching file
		for(int i=0; i<matchingFiles.length; i++)
		{
			File currentFile = matchingFiles[i];
			int depth = -1;
			
			System.out.println("\n\n=> Archivo:"+currentFile.toString()+"\n");
	        try {
	            PSTFile pstFile = new PSTFile(currentFile.toString());
	            processSubFolder(pstFile.getRootFolder(),depth,thunderMainFolder.toString(),pstFile.getMessageStore().getDisplayName());        
	        } catch (Exception err) {	err.printStackTrace();	}   
		}		
	}
    
    
    //Treat the PST structure, this is a recursive process
	private static void processSubFolder(PSTFolder folder,int depth, String mainFolder,String boxFolder) throws PSTException, java.io.IOException
    {
        depth++;
        if(depth==0)
        {
			//If it's the main folder create the main box with/out emails
        	if (folder.getContentCount() > 0) {				        	
				try {
					
					folderEmailsToMbox (folder,mainFolder,boxFolder);
					
				} catch (MessagingException e) {	e.printStackTrace();	}
				
			}else{
				File mainBox = new File(mainFolder+"/"+boxFolder);
		    	if (!mainBox.exists()) {
		    		mainBox.createNewFile();  	
		    	}
			}
				

        	if( folder.hasSubfolders() )
        	{
	        	 Vector<PSTFolder> childFolders = folder.getSubFolders();            
	             for ( PSTFolder childFolder : childFolders ) {
	                 processSubFolder(childFolder,depth,mainFolder,boxFolder);
	             }
        	}        	       	
        }
        else
        {	       
	        // go through the folders...
	        if ( folder.hasSubfolders() ) {
	            Vector<PSTFolder> childFolders = folder.getSubFolders();	            
	            File thunderFolder = new File( mainFolder+"/"+boxFolder+".sbd" );
	        	if ( !thunderFolder.exists() ) 
	        	{
	        		thunderFolder.mkdir(); 
	        	}
	            
	            for ( PSTFolder childFolder : childFolders ) {
	                processSubFolder( childFolder ,depth , thunderFolder.toString() , childFolder.getDisplayName() );
	            }
	        }
	
	        // and now the emails for this folder
	        if ( folder.getContentCount() > 0 ) {        	
	        	try {
	        		
					folderEmailsToMbox ( folder , mainFolder , boxFolder );
					
				} catch (MessagingException e) {	e.printStackTrace();	}           
	        }   
        }
        depth--;
    }
	
	//Analyze each folder looking for emails, process them and save to MBOX
	//@SuppressWarnings()
	
	private static void folderEmailsToMbox (PSTFolder folder,String mainFolder,String boxFolder) throws PSTException, IOException, MessagingException
	{
		//Declarations and getting the first email
		PSTMessage email = (PSTMessage)folder.getNextChild();        
        List<emailBasic> emailFolderList = new ArrayList<emailBasic>();
        
        //Loading PST emails
        while (email != null) {
        	emailBasic singleEmail  = new emailBasic();
        	//if(emailtemp.getMessageClass().contains("Conversation")){System.out.println("Conversation!! ");}       	
			singleEmail.addPST(email);
        	emailFolderList.add(singleEmail);
        	email = (PSTMessage)folder.getNextChild();  	        
        }  
        
        //Properties and creation of MBOX session
    	Properties prop = new Properties();		
    	//http://docs.oracle.com/javaee/6/api/javax/mail/internet/package-summary.html
    	//prop.setProperty("mail.mime.charset","utf8");//ISO-8859-1");
    	//prop.setProperty("mail.mime.setdefaulttextcharset","true");//intenta arreglar la codificacion
    	prop.setProperty("mail.mime.allowencodedmessages","true");//intenta arreglar adjuntos //mail.mime. ignoremultipartencoding
    	prop.setProperty("mail.mime.foldtext","false");
		prop.setProperty("mail.store.protocol", "mbox");
		prop.setProperty("mstor.mbox.metadataStrategy", "none");
		prop.setProperty("mstor.mbox.cacheBuffers", "disabled");
		prop.setProperty("mstor.cache.disabled", "true");
		prop.setProperty("mstor.mbox.bufferStrategy", "mapped");
		prop.setProperty("mstor.metadata", "enable");
				
		Session session = Session.getDefaultInstance( prop );
		Store store = session.getStore( new URLName( "mstor:"+mainFolder ) );
		store.connect();
		
		//Create or open box
        Folder mailFolder = store.getDefaultFolder().getFolder(boxFolder);
        System.out.println("\nBuzon: '"+boxFolder+"' en '"+mainFolder+"'");
        try {
        	mailFolder.create( Folder.HOLDS_MESSAGES | Folder.HOLDS_FOLDERS );
        	System.out.println("Buzon creado: "+mailFolder.getName());
        }catch(Exception e){
        	mailFolder.open( Folder.READ_WRITE );
        	System.out.println("Buzon abierto: "+mailFolder.getName());
        } 
        
		//Convert each mail in MBOX format and insert in Array
        Message message = new MimeMessage( session );
        Message[] messages = new Message[emailFolderList.size()];               
        int i = 0;
        for( emailBasic temp : emailFolderList )
        {
        	try {
        		message = temp.getMBOX( session );
        		messages[i] = message;
			} 
        	catch (IOException e) {	e.printStackTrace();	}
        	i++;     	
        }
        
        //Save loaded emails
        System.out.println("mensages a salvar: "+messages.length);
        mailFolder.appendMessages(messages);
        store.close();
        
	}
	
	
	
    
   
    /******************************************************************** MAIN *****************************************************************************/   	
	public static void main(String[] args) throws IOException {
		
		
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		
		Boolean error=false;
		String header = "\n\n"
+"	 .---.  .--. .-----.   .-.        .-..-..---.  .--. .-..-.\n" 
+"	 : .; :: .--'`-. .-'  .' `.       : `' :: .; :: ,. :: `' :\n" 
+"	 :  _.'`. `.   : :    `. .'.--.   : .. ::   .': :: : `  ' \n" 
+"	 : :    _`, :  : :     : :' .; :  : :; :: .; :: :; :.'  `.\n" 
+"	 :_;   `.__.'  :_;     :_;`.__.'  :_;:_;:___.'`.__.':_;:_;\n\n" 
+"\n"		
+"Bienvenido al conversor PST a MBOX (para Thunderbird).\n" 
+"\n"
+"\n"
+"Con el uso de este software usted puede convertir de forma automatica todos los archivos .pst contenidos en una carpeta.\n" 
+"\n"
+"Para ello deberemos indicarle la ruta donde se encuentran los .pst de origen as� como la ruta donde se crear� una carpeta llamada <localFolders> dentro de la que se volcar� todo el contenido ya convertido.\n"
+"IMPORTANTE: Deber�n mantenerse cerrados los clientes de correo.\n"
+"\n\n"
+"   Ejemplo de uso: C:\\>PstToMbox.exe \"C:\\<origen>\" \"C:\\<destino>\"\n"
+"\n\n"
+"IMPORTANTE: La carpeta <destino> deber� existir.\n"
+"En este ejemplo, en la carpeta <destino> se crear� una carpeta \"localFolder\", la que podr� copiar en el lugar donde se almacenen sus correos archivados de Thunderbird.\n"
+"En caso de ser la carpeta por defecto, puede encontrarla en C:\\Users\\<Windows user name>\\AppData\\Roaming\\Thunderbird\\Profiles\\<Profile name>\\Mail, dentro podr� localizar una carpeta localFolders en la cual puede copiar el contenido generado en <destino>\\localFolders.\n\n"
+"Una vez copiado todo el contenido desde <destino>\\localFolder\\, puede abrir Thunderbird y los correos deber�n aparecerle ya en las carpetas correspondientes.\n";


		System.out.println(header);
		//verify argument exists
		if(args.length==2)
		{
			
			
			String strFolder = args[0].replace("\"", "").replace('\\', '/');			
			File mainFolder = new File(strFolder);
			if(mainFolder.exists()&&!strFolder.endsWith(".pst"))
			{
				
				strFolder = args[1].replace("\"", "").replace('\\', '/');		
				File destinationFolder = new File(strFolder);
				if(destinationFolder.exists())
				{
					final PrintStream err = new PrintStream(System.err);
					
					//ERRORS TO FILE:
					File errorFile = File.createTempFile("PstToMbox",".log");//create file in Windows %TMP% folder
			    	if ( !errorFile.exists() ) 
			    	{	errorFile.createNewFile();	}
			    	else{errorFile.setWritable(true);}
					System.setErr(new PrintStream(errorFile));
					
					System.out.println("\n****************************   Proceso Iniciado   ******************************\n");
					scanPst(mainFolder,destinationFolder);
					
					System.setErr(err);
				}else{	error=true; System.out.println("\nERROR: La carpeta <destino> indicada: < "+args[1]+" > no existe o es inaccesible, por favor, verifique la ruta e intentelo de nuevo.");	}
				
				
			}else{	error=true; System.out.println("\nERROR: La carpeta <origen> indicada: < "+args[0]+" > no existe o es inaccesible, por favor, verifique la ruta e intentelo de nuevo.");	}
			
		}else{	error=true; System.out.println("\n\nERROR: No se han indicado correctamente las carpetas origen y destino.");}
		
		
		
		if(!error){System.out.println("\n****************************  Proceso Finalizado  ******************************");}
		else{System.out.println("\n*********************** Proceso Finalizado con Errores *************************");}
			
	}
}
